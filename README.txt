Welcome to this Python AI Project
-------------------------------------------
Created by :- Nathan a.k.a. DarthBacon
-------------------------------------------
Current Version(s) :- 0.1 / init. release
Previous Version(s) :- n/a
-------------------------------------------
Overview :
____This is an open source project that is meant to be a cool group of python modules all rolled into one. 
____It will mainly be a command line interface (CLI) project that utilises many different modules.
____The reason this is being made is because I was really bored and didn't know what to do.
-------------------------------------------
Project Files :
- Top level (repo)/
 - README.txt
 - v01/
  - v01README.txt
  - python-files/
   - eventlog.log _The log of events
   - main.py _Run This.
   - modulepack/ _Modules go in here.
    - __pycache__/ _Don't mess with this.
    - __init__.py _Add your own module names here.
    - startandend.py _Startup and exit stuff.
    - loop.py _The main loop of the thingy.