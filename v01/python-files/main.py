from modulepack import *

# Start the application and display the welcome message
startandend.startup('Welcome to the Python AI Project, v0.1!')

# Start the main loop
loop.loop()

# End the application
startandend.end('You are now exiting this application!')
