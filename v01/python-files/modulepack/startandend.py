import logging
import os, sys
from time import sleep

# Start the application and log the event!
def startup(message='Welcome!'):
    print(message)
    logging.basicConfig(filename='eventlog.log', level=logging.DEBUG)
    logging.info(' Application Started.')
    sleep(2)

# End the application and log the event
def end(message='Goodbye!'):
    print(message)
    logging.basicConfig(filename='eventlog.log', level=logging.DEBUG)
    logging.info(' Application Closed.')
    sleep(2)
    sys.exit(0)
