import logging
import os, sys

def loop(prompt='$ '):
    logging.basicConfig(filename='eventlog.log', level=logging.DEBUG)
    logging.info(' Loop started.')
    print('Type "Can I have help" or any other thing containing the word help for help!')
    while True:
        cmd = input(prompt)
        cmd_raw = cmd.split(' ')
        if any('exit' in c for c in cmd_raw) or any('Exit' in c for c in cmd_raw) or any('EXIT' in c for c in cmd_raw):
            break

        if any('help' in c for c in cmd_raw) or any('Help' in c for c in cmd_raw) or any('HELP' in c for c in cmd_raw):
            hlp()

        if any('information' in c for c in cmd_raw) or any('Information' in c for c in cmd_raw) or any('INFORMATION' in c for c in cmd_raw):
            about()

        if any('clear' in c for c in cmd_raw) or any('Clear' in c for c in cmd_raw) or any('CLEAR' in c for c in cmd_raw):
            os.system('cls')
            logging.info(' Cleared the screen.')
                        
        print()

def hlp():
    logging.info(' Help menu opened.')
    print('This is the help menu!')
    print('Type the following commands in your sentences to do it:')
    print('SYSTEM COMMANDS!')
    print('Exit - Exit the application. I don\'t know why you would want to.')
    print('Help - How did you get here if you didn\'t know this!')
    print('Information - Get info about the application.')
    print('Clear - Clear all previous commands from the screen.')

def about():
    logging.info(' Info menu opened.')
    print('This is the information menu.')
    print('Currently you are using v0.1.')
